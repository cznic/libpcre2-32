// Copyright 2023 The libpcre2-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"modernc.org/cc/v4"
	util "modernc.org/ccgo/v3/lib"
	ccgo "modernc.org/ccgo/v4/lib"
)

const (
	archivePath = "pcre2-10.42.tar.gz"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	sed    = "sed"
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	if ccgo.IsExecEnv() {
		if err := ccgo.NewTask(goos, goarch, os.Args, os.Stdout, os.Stderr, nil).Main(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	f, err := os.Open(archivePath)
	if err != nil {
		fail(1, "cannot open tar file: %v\n", err)
	}

	f.Close()

	_, extractedArchivePath := filepath.Split(archivePath)
	extractedArchivePath = extractedArchivePath[:len(extractedArchivePath)-len(".tar.gz")]
	tempDir := os.Getenv("GO_GENERATE_DIR")
	dev := os.Getenv("GO_GENERATE_DEV") != ""
	switch {
	case tempDir != "":
		util.MustShell(true, "sh", "-c", fmt.Sprintf("rm -rf %s", filepath.Join(tempDir, extractedArchivePath)))
	default:
		var err error
		if tempDir, err = os.MkdirTemp("", "libpcre2-32-generate"); err != nil {
			fail(1, "creating temp dir: %v\n", err)
		}

		defer func() {
			switch os.Getenv("GO_GENERATE_KEEP") {
			case "":
				os.RemoveAll(tempDir)
			default:
				fmt.Printf("%s: temporary directory kept\n", tempDir)
			}
		}()
	}
	libRoot := filepath.Join(tempDir, extractedArchivePath)
	makeRoot := libRoot
	fmt.Fprintf(os.Stderr, "archivePath %s\n", archivePath)
	fmt.Fprintf(os.Stderr, "extractedArchivePath %s\n", extractedArchivePath)
	fmt.Fprintf(os.Stderr, "tempDir %s\n", tempDir)
	fmt.Fprintf(os.Stderr, "libRoot %s\n", libRoot)
	fmt.Fprintf(os.Stderr, "makeRoot %s\n", makeRoot)

	util.MustShell(true, "tar", "xzf", archivePath, "-C", tempDir)
	util.MustCopyFile(true, "LICENCE-PCRE2.md", filepath.Join(libRoot, "LICENCE"), nil)
	os.RemoveAll("testdata")
	result := filepath.Join("libpcre2.go")
	util.MustInDir(true, makeRoot, func() (err error) {
		cflags := []string{
			// "-UNDEBUG", //TODO-
		}
		if s := cc.LongDouble64Flag(goos, goarch); s != "" {
			cflags = append(cflags, s)
		}
		util.MustShell(true, "sh", "-c", "go mod init example.com/libpcre2-32 ; go get modernc.org/libc@latest")
		if dev {
			util.MustShell(true, "sh", "-c", "go work init ; go work use $GOPATH/src/modernc.org/libc")
		}
		util.MustShell(true, "sh", "-c", fmt.Sprintf("CFLAGS='%s' ./configure "+
			"--disable-jit "+
			"--disable-pcre2-16 "+
			"--disable-pcre2-8 "+
			"--disable-pcre2grep-callout "+
			"--disable-pcre2grep-callout-fork "+
			"--disable-pcre2grep-jit "+
			"--disable-shared "+
			"--enable-pcre2-32 ",
			strings.Join(cflags, " "),
		))
		args := []string{os.Args[0]}
		if dev {
			args = append(
				args,
				"-absolute-paths",
				"-keep-object-files",
				"-positions",
			)
		}
		args = append(args,
			"--libc", "modernc.org/libc",
			"--prefix-enumerator=_",
			"--prefix-external=x_",
			"--prefix-field=F",
			"--prefix-macro=m_",
			"--prefix-static-internal=_",
			"--prefix-static-none=_",
			"--prefix-tagged-enum=_",
			"--prefix-tagged-struct=T",
			"--prefix-tagged-union=T",
			"--prefix-typename=T",
			"--prefix-undefined=_",
			"-extended-errors",
		)
		if err := ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "check"), os.Stdout, os.Stderr, nil).Exec(); err != nil {
			return err
		}

		if err := ccgo.NewTask(goos, goarch, append(args,
			"-o", result,
			"--package-name", "libpcre2_32",
			filepath.Join(".libs", "libpcre2-32.a"),
		), os.Stdout, os.Stderr, nil).Main(); err != nil {
			return err
		}

		util.MustShell(true, "sed", "-i", `s/\<T__\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/t__\1/g`, result)
		util.MustShell(true, "sed", "-i", `s/\<x_\([a-zA-Z0-9][a-zA-Z0-9_]\+\)/X\1/g`, result)

		return nil
	})

	fn := fmt.Sprintf("ccgo_%s_%s.go", goos, goarch)
	util.MustCopyFile(false, fn, filepath.Join(makeRoot, result), nil)
	util.MustCopyFile(false, filepath.Join("internal", "pcre2test", fn), filepath.Join(makeRoot, "pcre2test.go"), nil)
	util.MustCopyDir(false, filepath.Join("internal", "testdata"), filepath.Join(makeRoot, "testdata"), nil)
	fn = filepath.Join("internal", "RunTest")
	util.MustCopyFile(false, fn, filepath.Join(makeRoot, "RunTest"), nil)
	// Disable tests for locales modernc.org/libc does not support
	util.MustShell(true, sed, "-i", `s/if \[ \$do3 = yes \] ; then/if \[ \$do3 = disabled \] ; then/`, fn)
}
