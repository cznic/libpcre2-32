// Copyright 2023 The libpcre2-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libpcre2_32 is a ccgo/v4 version of libpcre2-32.a (https://www.pcre.org)
package libpcre2_32 // import "modernc.org/libpcre2-32"
